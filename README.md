# gettext

GNU Internationalization utilities. [www.gnu.org/software/gettext](http://www.gnu.org/software/gettext)

* [*The environment variable TERM*
  ](https://www.gnu.org/software/gettext/manual/html_node/The-TERM-variable.html)
* [*term(7) - Linux man page*
  ](https://linux.die.net/man/7/term)
